import express from "express";
import serveIndex from "serve-index";


function start_serving(path, port) {
    const app = express();
    app.use(express.static(path), serveIndex(path));
    console.log('Listening to port: ', port);
    app.listen(port);
}


start_serving("/home/crypt/Downloads/torrents", "8000");

console.log('Hello, world!')
